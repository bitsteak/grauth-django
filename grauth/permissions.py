from rest_framework import permissions

_allowed_views = {
    'create': ('post',),
    'forgot_password': ('post',),
    'reset_password': ('post',),
    'email_confirmation': ('get',)
}


class IsAllowedOrAuthenticated(permissions.BasePermission):

    def has_permission(self, request, view):
        if not request.user.is_authenticated():
            if view.action in _allowed_views:
                return request.method.lower() in _allowed_views[view.action]
            else:
                return False
        else:
            return True


class IsAdminUser(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.is_admin


class IsStaffUser(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user.is_staff
