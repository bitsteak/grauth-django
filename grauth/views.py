from django.shortcuts import get_object_or_404
from django.db.models import Q
from rest_framework_expiring_authtoken.models import ExpiringToken
from .models import Person
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet
from rest_framework.decorators import list_route
from permissions import IsAllowedOrAuthenticated
from serializers import PersonCreatorSerializer, PersonUpdateProfileSerializer, PersonProfileSerializer, \
    PersonUpdatePasswordSerializer, PasswordResetSerializer


class PersonViewSet(ViewSet):
    queryset = Person.objects.none()
    permission_classes = (IsAllowedOrAuthenticated, )

    def create(self, request):
        data = dict(request.data.iteritems())
        serializer = PersonCreatorSerializer(data=data)
        if serializer.is_valid():
            user = Person.objects.create_user(
                email=serializer.initial_data['email'],
                first_name=serializer.initial_data['first_name'],
                last_name=serializer.initial_data['last_name'],
                password=serializer.initial_data['password']
            )
            token, _ = ExpiringToken.objects.get_or_create(user=user)
            return Response({'token': token.key}, status=status.HTTP_200_OK)
        return Response(serializer.errors)

    @list_route(
        methods=['post', 'get'],
    )
    def profile(self, request):
        if request.method == 'POST':
            data = dict(request.data.iteritems())
            serializer = PersonUpdateProfileSerializer(data=data, instance=request.user)
            if serializer.is_valid():
                serializer.save()
                return Response({'status': 'OK'}, status=status.HTTP_200_OK)
            return Response(serializer.errors)
        if request.method == 'GET':
            queryset = get_object_or_404(Person, email=request.user.email)
            serializer = PersonProfileSerializer(queryset, many=False)
            return Response(serializer.data)
        return Response({'status': 'Method not allowed'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    @list_route(
        methods=['post', ],
        url_path='update-password',
    )
    def update_password(self, request):
        data = dict(request.data.iteritems())
        serializer = PersonUpdatePasswordSerializer(data=data)
        if serializer.is_valid():
            person = request.user
            person.set_password(serializer.validated_data['new_password'])
            person.save()
            token = person.get_or_create_token()

            return Response({'token': token.key}, status=status.HTTP_200_OK)
        return Response(serializer.errors)

    @list_route(
        methods=['get', ],
        url_path='search',
    )
    def search_person(self, request):
        if not request.user.is_staff:
            return Response({'status': 'Permission denied'}, status=status.HTTP_401_UNAUTHORIZED)
        custom_filter = Q()
        for key, item in request.query_params.iteritems():
            custom_filter |= Q(**{key+'__contains': item})
        queryset = Person.objects.filter(custom_filter).exclude(is_staff=True)
        serializer = PersonProfileSerializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(
        methods=['post', 'get'],
        url_path='email-confirmation'
    )
    def email_confirmation(self, request):
        if request.method == 'POST':
            request.user.send_email_confirmation()
            return Response({'status': 'OK'}, status=status.HTTP_200_OK)
        if request.method == 'GET':
            activation_key = request.query_params.get('key', None)
            if not activation_key:
                return Response({'key': 'Missing activation key'}, status=status.HTTP_400_BAD_REQUEST)
            user = get_object_or_404(Person, email_activation_key=activation_key)
            return Response(user.validate_email(activation_key), status=status.HTTP_200_OK)
        return Response({'status': 'Method not allowed'}, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    @list_route(
        methods=['post'],
        url_path='forgot-password'
    )
    def forgot_password(self, request):
        email = request.data.get('email', None)
        if not email:
            return Response({'email': 'Missing email value'}, status=status.HTTP_400_BAD_REQUEST)
        user = get_object_or_404(Person, email=email)
        user.send_password_email_reset()
        return Response({'status': 'OK'}, status=status.HTTP_200_OK)

    @list_route(
        methods=['post'],
        url_path='reset-password'
    )
    def reset_password(self, request):
        data = dict(request.data.iteritems())
        serializer = PasswordResetSerializer(data=data)
        if serializer.is_valid():
            person = get_object_or_404(Person, password_reset_user_reference=serializer.validated_data['user'])
            return Response(
                person.validate_password_reset(
                    serializer.validated_data['key'],
                    serializer.validated_data['new_password']
                ), status=status.HTTP_200_OK)
        return Response(serializer.errors)

    @list_route(
        methods=['post', 'get']
    )
    def logout(self, request):
        request.user.reset_token()
        return Response({'response': 'Logout executado com sucesso'}, status=status.HTTP_200_OK)
