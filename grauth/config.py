from django.apps import AppConfig
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings


class GRAuthConfig(AppConfig):
    name = 'grauth'

    def ready(self):
        import signals
        post_save.connect(receiver, sender=settings.AUTH_USER_MODEL)
