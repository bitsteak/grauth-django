# -*- coding:UTF-8 -*-
from rest_framework import serializers
from .models import Person
import django.contrib.auth.password_validation as validators
from django.core import exceptions


class PersonCreatorSerializer(serializers.ModelSerializer):
    def validate(self, attrs):
        try:
            validators.validate_password(attrs['password'])
        except exceptions.ValidationError as e:
            raise serializers.ValidationError({'password': ['Senha inválida']})
        return attrs

    class Meta:
        model = Person
        fields = (
            'email',
            'first_name',
            'last_name',
            'password'
        )


class PersonUpdateProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = (
            'email',
            'first_name',
            'last_name',
        )


class PersonProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = (
            'email',
            'first_name',
            'last_name',
        )


class PersonUpdatePasswordSerializer(serializers.Serializer):
    new_password = serializers.CharField()
    new_password2 = serializers.CharField()

    def validate(self, attrs):
        if attrs['new_password'] != attrs['new_password2']:
            raise serializers.ValidationError({'password': ['A confirmação de senha e a senha são diferentes.']})
        try:
            validators.validate_password(attrs['new_password'])
        except exceptions.ValidationError as e:
            raise serializers.ValidationError({'password': ['Senha inválida']})
        return attrs


class PasswordResetSerializer(serializers.Serializer):
    new_password = serializers.CharField()
    new_password2 = serializers.CharField()
    user = serializers.CharField()
    key = serializers.CharField()

    def validate(self, attrs):
        if attrs['new_password'] != attrs['new_password2']:
            raise serializers.ValidationError({'password': ['A confirmação de senha e a senha são diferentes.']})
        try:
            validators.validate_password(attrs['new_password'])
        except exceptions.ValidationError as e:
            raise serializers.ValidationError({'password': ['Senha inválida']})
        return attrs
