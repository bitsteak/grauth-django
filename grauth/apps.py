from __future__ import unicode_literals

from django.apps import AppConfig


class GrauthConfig(AppConfig):
    name = 'grauth'
