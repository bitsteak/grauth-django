from django.conf.urls import url, include
from rest_framework import routers
from rest_framework_expiring_authtoken import views
from .views import PersonViewSet


router = routers.DefaultRouter()
router.register(r'users', PersonViewSet)

urlpatterns = [
    url(r'^users/login/', views.obtain_expiring_auth_token),
    url(r'^', include(router.urls)),
]
