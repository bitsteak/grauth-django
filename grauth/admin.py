from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from .models import Person


class PersonCreationForm(forms.ModelForm):
    # Create a custom form to create a new Person.
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    class Meta:
        model = Person
        fields = (
            'email',
            'first_name',
            'last_name',
        )

    def save(self, commit=True):
        user = super(PersonCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user


class PersonChangeForm(forms.ModelForm):
    # Form to update user
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Person
        fields = (
            'email',
            'first_name',
            'last_name',
            'is_admin',
            'email_confirmed',
        )

    def clean_password(self):
        return self.initial['password']


class PersonAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = PersonChangeForm
    add_form = PersonCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'first_name', 'is_admin')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'email_confirmed')}),
        ('Permissions', {'fields': ('is_admin', 'is_staff', 'groups')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password')}
         ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


admin.site.register(Person, PersonAdmin)
