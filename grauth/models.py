# -*- coding:UTF-8 -*-
from __future__ import unicode_literals
import hashlib
import uuid
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.core import exceptions
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone
from rest_framework_expiring_authtoken.models import ExpiringToken

from grauth_settings import grauth_settings


email_validation_messages = {
    'validated': {'status': 'Successfully validated'},
    'invalid': {'status': 'Invalid activation key.'},
    'expired': {'status': 'Expired activation key.'},
}

password_reset_messages = {
    'success': {'status': 'Successfully reseted'},
    'invalid': {'status': 'Invalid reset key.'},
    'expired': {'status': 'Expired reset key.'},
}


class PersonManager(BaseUserManager):

    def create_user(self, email, first_name, last_name, password=None):
        if not email:
            raise ValueError('Person must have an email')

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(
            email=email,
            password=password,
            first_name='',
            last_name=''
        )
        user.is_admin = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class Person(AbstractBaseUser, PermissionsMixin):
    email = models.CharField("Email", max_length=255, unique=True)
    first_name = models.CharField("Nome", max_length=100)
    last_name = models.CharField("Sobrenome", max_length=100)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    email_confirmed = models.BooleanField(default=False)
    email_activation_key = models.CharField(max_length=40, blank=True, null=True)
    email_activation_key_created_at = models.DateTimeField(blank=True, null=True)

    password_reset_key = models.CharField(max_length=40, blank=True, null=True)
    password_reset_user_reference = models.CharField(max_length=40, blank=True, null=True)
    password_reset_key_created_at = models.DateTimeField(blank=True, null=True)

    objects = PersonManager()

    USERNAME_FIELD = 'email'

    class Meta:
        verbose_name = 'Pessoa'
        verbose_name_plural = 'Pessoas'

    def get_full_name(self):
        # Person is identified by first_name and last_name
        return "%s %s" % (self.first_name, self.last_name)

    def get_short_name(self):
        # Person is identified by first_name and last_name
        return self.first_name

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        # Does the user have a specific permission?
        # Simplest possible answer: Yes, always
        return self.is_staff

    def has_module_perms(self, app_label):
        # Does the user have permissions to view the app `app_label`?
        # Simplest possible answer: Yes, always
        return self.is_staff

    def send_email_confirmation(self):
        if not self.email_confirmed:
            self.email_activation_key = hashlib.sha1(str(timezone.now())).hexdigest()
            self.email_activation_key_created_at = timezone.now()
            self.save()
            send_mail(
                grauth_settings.GRAUTH_EMAIL_CONFIRMATION_SUBJECT,
                'Clique no link para ativar sua conta. %s' % self.create_email_validation_url(),
                grauth_settings.GRAUTH_EMAIL_SENDER,
                [self.email],
                fail_silently=False,
            )

    def create_email_validation_url(self):
        return grauth_settings.GRAUTH_PROJECT_DOMAIN + \
               'email-confirmation?key=' + self.email_activation_key

    def validate_email(self, activation_key):
        now = timezone.now()
        if not self.email_activation_key_created_at < now - grauth_settings.GRAUTH_EMAIL_ACTIVATION_KEY_LIFESPAN:
            if activation_key == self.email_activation_key:
                self.email_confirmed = True
                self.save()
                return email_validation_messages['validated']
            return email_validation_messages['invalid']
        return email_validation_messages['expired']

    def send_password_email_reset(self):
        salt = uuid.uuid4().hex
        self.password_reset_key = hashlib.sha1(salt.encode() + str(timezone.now())).hexdigest()
        self.password_reset_user_reference = hashlib.sha1(salt.encode() + str(self.email)).hexdigest()
        self.password_reset_key_created_at = timezone.now()
        self.save()
        send_mail(
            grauth_settings.GRAUTH_PASSWORD_RESET_SUBJECT,
            'Clique no link para redefinir a senha. %s' % self.create_reset_url_password(),
            grauth_settings.GRAUTH_EMAIL_SENDER,
            [self.email],
            fail_silently=False,
        )

    def create_reset_url_password(self):
        return grauth_settings.GRAUTH_PROJECT_DOMAIN + \
               'reset-password?user=' + self.password_reset_user_reference + \
               '&key=' + self.password_reset_key

    def validate_password_reset(self, reset_key, new_password):
        now = timezone.now()
        if not self.password_reset_key_created_at < now - grauth_settings.GRAUTH_PASSWORD_RESET_KEY_LIFESPAN:
            if reset_key == self.password_reset_key:
                self.set_password(new_password)
                self.save()
                self.reset_token()
                return password_reset_messages['success']
            return password_reset_messages['invalid']
        return password_reset_messages['expired']

    def get_or_create_token(self):
        token, created = ExpiringToken.objects.get_or_create(user=self)
        if not created:
            token.delete()
            token = ExpiringToken.objects.create(user=self)
        return token

    def reset_token(self):
        try:
            ExpiringToken.objects.get(user=self).delete()
        except exceptions.ObjectDoesNotExist:
            pass
        return ExpiringToken.objects.create(user=self)
