from datetime import timedelta
from django.conf import settings


class GRAuthSettings(object):

    @property
    def GRAUTH_EMAIL_ACTIVATION_KEY_LIFESPAN(self):
        """
        Return the allowed lifespan of a email activation key as a TimeDelta object.

        Defaults to 30 days.
        """
        try:
            val = settings.GRAUTH_EMAIL_ACTIVATION_KEY_LIFESPAN
        except AttributeError:
            val = timedelta(days=30)
        return val

    @property
    def GRAUTH_EMAIL_SENDER(self):
        """
        Return the email confirmation sender.

        Defaults to grauth@email.com.
        """
        try:
            val = settings.GRAUTH_EMAIL_SENDER
        except AttributeError:
            val = 'grauth@email.com'
        return val

    @property
    def GRAUTH_EMAIL_CONFIRMATION_SUBJECT(self):
        """
        Return the email confirmation subject.

        Defaults to 'Activate your GRAuth account'.
        """
        try:
            val = settings.GRAUTH_EMAIL_CONFIRMATION_SUBJECT
        except AttributeError:
            val = 'Activate your GRAuth account'
        return val

    @property
    def GRAUTH_PASSWORD_RESET_KEY_LIFESPAN(self):
        """
        Return the allowed lifespan of a password reset key as a TimeDelta object.

        Defaults to 1 day.
        """
        try:
            val = settings.GRAUTH_PASSWORD_RESET_KEY_LIFESPAN
        except AttributeError:
            val = timedelta(days=1)
        return val

    @property
    def GRAUTH_PASSWORD_RESET_SUBJECT(self):
        """
        Return the password reset email subject.

        Defaults to 'Reset your GRAuth password'.
        """
        try:
            val = settings.GRAUTH_PASSWORD_RESET_SUBJECT
        except AttributeError:
            val = 'Reset your GRAuth password'
        return val

    @property
    def GRAUTH_PROJECT_DOMAIN(self):
        """
        Return the reset url.

        """
        try:
            val = settings.GRAUTH_PROJECT_DOMAIN
            if str.endswith(val, '/'):
                return val + 'grauth/users/'
            return val + '/grauth/users/'
        except AttributeError:
            raise AttributeError('You must set the reset password URL')

grauth_settings = GRAuthSettings()
